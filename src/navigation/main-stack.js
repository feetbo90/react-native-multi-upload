import React from 'react';

import {mainStack} from '../config/navigator';

import {createStackNavigator} from '@react-navigation/stack';

import Home from '../screens/home';
import ListImage from '../screens/list-image';


const Stack = createStackNavigator();

function MainStack() {
  return (
    <Stack.Navigator
      initialRouteName={mainStack.home_main}
      screenOptions={{gestureEnabled: false}}>
      
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.home_main}
        component={Home}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.list_image}
        component={ListImage}
      />
    </Stack.Navigator>
  );
}

export default MainStack;
