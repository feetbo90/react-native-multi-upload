/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Button
} from 'react-native';


import {
  Colors
} from 'react-native/Libraries/NewAppScreen';
import RNFetchBlob from 'react-native-fetch-blob'
import {launchImageLibrary} from 'react-native-image-picker';
// const { fs, fetch, wrap } = RNFetchBlob

const PilihMenu = () => {
  
  const [avatarSource, setAvatarSource] = useState(null);
  const [namaFile, setNamaFile] = useState(null);

  const { fs, fetch, wrap } = RNFetchBlob

  const dirs = fs.dirs
  // console.log(dirs.DocumentDir)
  // console.log(dirs.CacheDir)
  // console.log(dirs.DCIMDir)
  // console.log(dirs.DownloadDir)
  let filePath = `${dirs.DocumentDir}/RnUpload`
  let filePathDelete = `${dirs.DocumentDir}/RnUpload/*`

  let filePathTest = `${dirs.DocumentDir}/RnUpload/rn_image_picker_lib_temp_db57ae21-a37d-4a31-95b0-b5aa4aaaaaca.jpg`
  
  // buat folder
  // fs.mkdir(filePath)
  // .then((response) => {console.log("Berhasil buat" + JSON.stringify(response))})
  // .catch((err) => {console.log("gagal : " + err.message)})

  // buat file
  // fs.createFile(filePathTest, 'iqbal memang ganteng', 'utf8')
  // .then((response) => {console.log("Berhasil buat file : " + JSON.stringify(response))})
  // .catch((err) => {console.log("gagal : " + JSON.stringify(err.message))})

  // baca file
  // RNFetchBlob.fs.readFile(filePathTest, 'base64')
  // .then((data) => {
  //   // handle the data ..
  //   console.log("ini data cuk: " + JSON.stringify(data))
  // })
  // .catch((err) => {console.log("ini error : " + JSON.stringify(err.message))})

  // console.log('Files list in TRACK_FOLDER = ', RNFetchBlob.fs.ls(filePath));

  RNFetchBlob.fs.ls(filePath)
  .then( (files) =>{ 
    console.log(files.length);  
    console.log(files); 
    console.log(files[0]); 

  })

  // RNFetchBlob.fs.unlink(filePathDelete)
  // .then(() => { console.log("berhasil") })
  // .catch((err) => { console.log("ini error : " + JSON.stringify(err.message))  })

  const selectPhotoTapped = () => {
    
    const options = {
      noData: true,
      includeBase64: true,
      path: 'images',
    };

    launchImageLibrary(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }

     
        if (response.uri) {
          setAvatarSource(response)
          
          setNamaFile(response.fileName)
        }
    })
  }

  const handleUploadPhoto = () => {
    fetch('http://localhost:3000/api/upload', {
      method: 'POST',
      body: createFormData(avatarSource, { userId: '123' }),
    })
      .then((response) => response.json())
      .then((response) => {
        console.log('upload succes', response);
        alert('Upload success!');
        setAvatarSource({ photo: null });
      })
      .catch((error) => {
        console.log('upload error', error);
        alert('Upload failed!');
      });
  };

  const handleUploadBlob = async () => {
    let path = `${dirs.DocumentDir}/RnUpload/${namaFile}`;
    await fs.createFile(path, avatarSource.base64 , 'base64')
      .then(() => {console.log("Berhasil buat file : " )})
      .catch((err) => {console.log("gagal : " + JSON.stringify(err.message))})

  }

  const createFormData = (photo, body) => {
    const data = new FormData();
  
    data.append('photo', {
      name: photo.fileName,
      type: photo.type,
      uri:
        Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
    });
  
    Object.keys(body).forEach((key) => {
      data.append(key, body[key]);
    });
  
    return data;
  };
  
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      {avatarSource && (
        <React.Fragment>
          <Image
            source={{ uri: avatarSource.uri }}
            style={{ width: 300, height: 300 }}
          />
          <Button title="Upload" onPress={handleUploadBlob} />
        </React.Fragment>
      )}
      <Button title="Choose Photo" onPress={selectPhotoTapped} />
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default PilihMenu;
