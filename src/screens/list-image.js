import React from 'react';
import {
    View,
    Text,
    Button,
    StyleSheet
  } from 'react-native';
  import mainStack from '../config/navigator';


const ListImage = ({navigation}) => {
    
    return ( 
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>List Image</Text>
            
        </View>
    )
}
const styles = StyleSheet.create({
    buttonStyle: {
        color: 'red',
        marginTop: 20,
        padding: 20,
        backgroundColor: 'green'
    }
});
export default ListImage;