import React from 'react';
import {
    View,
    Text,
    Button,
    StyleSheet
  } from 'react-native';
import {mainStack} from '../config/navigator';

const Home = ({navigation}) => {
    // const { navigation } = props;

    const pindahHalaman = () => {
        navigation.navigate(mainStack.list_image)
    }

    return ( 
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home</Text>
        
            <Button 
                style={styles.buttonStyle}
                onPress={pindahHalaman}
                title="List Image" />
        </View>
    )
}

const styles = StyleSheet.create({
    buttonStyle: {
        color: 'red',
        marginTop: 20,
        padding: 20,
        backgroundColor: 'green'
    }
});

export default Home;